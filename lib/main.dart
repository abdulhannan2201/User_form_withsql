import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart'as path;
import 'package:user_info_inputs/helpers/db_helpers.dart';
import 'package:path_provider/path_provider.dart' as syspaths;

void main() {
  runApp(MaterialApp(debugShowCheckedModeBanner: false, home: MyTextField()));
}

class MyTextField extends StatefulWidget {
  const MyTextField({Key? key}) : super(key: key);

  @override
  State<MyTextField> createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {
  File? userimage;
  TextEditingController textusername = TextEditingController();
  TextEditingController textuseremail = TextEditingController();
  TextEditingController textusercnic = TextEditingController();
  TextEditingController textuseradd = TextEditingController();
  TextEditingController textusergender = TextEditingController();

  Future<void> fatchandsetUser() async {
    final data = await DBHelper.fatchData('user_info');
    textusername.text = data[0]['name'].toString();
    textusergender.text = data[0]['gender'].toString();
    textuseremail.text = data[0]['email'].toString();
    textusercnic.text = data[0]['cnic'].toString();
    textuseradd.text = data[0]['address'].toString();
    setState(() {
      userimage = File(data[0]['image'].toString());
    });
    print(userimage);
  }

  Future<void> _takeImage() async {
    final imageFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 600,
    );
    if (imageFile == null) {
      return;
    }
    setState(() {
      userimage = File(imageFile.path);
    });
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> Snackbar( BuildContext context){
    return Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content: Row(
                                    children: const [
                                      Icon(
                                        Icons.no_accounts,
                                        size: 30,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text('Your Info In saved'),
                                    ],
                                  ),
                                ),
                              );
  }

  @override
  void initState() {
    super.initState();
    fatchandsetUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("USER INPUTS"),
        backgroundColor: Colors.tealAccent,
      ),
      body: GestureDetector(
        onTap: FocusScope.of(context).unfocus,
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
              child: RefreshIndicator(
                onRefresh: () => fatchandsetUser(),
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 50),
                      child: Center(
                        child: Stack(
                          children: [
                            Container(
                                width: 150,
                                height: 150,
                                decoration:  BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  boxShadow: [
                                    BoxShadow(
                                      color:Colors.tealAccent,
                                      spreadRadius: 20,
                                      blurRadius: 70,
                                    )
                                  ],
                                  // shape: BoxShape.circle,
                                ),
                                child: userimage != null ? Image.file(userimage!,fit: BoxFit.cover,): Icon(Icons.camera_alt),
                                
                                ),
                            Positioned(
                              // top: 0,
                              bottom: 0,
                              right: 0,
                              child: Container(
                                height: 40,
                                width: 40,
                                decoration:const BoxDecoration(
                                    color: Colors.tealAccent,
                                    shape: BoxShape.circle),
                                child: IconButton(
                                  onPressed: () =>_takeImage(),
                                  
                                  icon: Icon(Icons.edit),
                                  color: Colors.teal,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        style: TextStyle(color: Colors.black87),
                        controller: textusername,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(bottom: 3),
                            labelText: "UserName",
                            prefixIcon: const Icon(
                              Icons.person,
                              color: Colors.teal,
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            hintText: textusername.text,
                            hintStyle: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            labelStyle: const TextStyle(color: Colors.black54)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        style: TextStyle(color: Colors.black87),
                        controller: textusergender,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(bottom: 3),
                            labelText: "Gender",
                            prefixIcon: const Icon(
                              Icons.people,
                              color: Colors.teal,
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            hintText: textusergender.text,
                            hintStyle: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            labelStyle: const TextStyle(color: Colors.black54)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        style: TextStyle(color: Colors.black87),
                        controller: textuseremail,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(bottom: 3),
                            labelText: "Email",
                            prefixIcon: const Icon(
                              Icons.mail,
                              color: Colors.teal,
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            hintText: textuseremail.text,
                            hintStyle: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            labelStyle: const TextStyle(color: Colors.black54)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        style: TextStyle(color: Colors.blue),
                        controller: textusercnic,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(bottom: 3),
                            labelText: "CNIC",
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.teal,
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            hintText: textusercnic.text,
                            hintStyle: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            labelStyle: const TextStyle(color: Colors.black54)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        style: TextStyle(color: Colors.black54),
                        controller: textuseradd,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(bottom: 3),
                            labelText: "Address",
                            prefixIcon: Icon(
                              Icons.location_city,
                              color: Colors.teal,
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            hintText: textuseradd.text,
                            hintStyle: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            labelStyle: TextStyle(color: Colors.black54)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.teal,
                            elevation: 4,
                          ),
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            try {
                              DBHelper.insert('user_info', {
                                'id': 1,
                                'name': textusername.text,
                                'gender': textusergender.text,
                                'cnic': textusercnic.text,
                                'email': textuseremail.text,
                                'address': textuseradd.text,
                                'image': userimage!.path
                              });
                              Snackbar(context);
                              
                            } catch (e) {
                              throw Exception(e.toString());
                            }
                          },
                          child: const Text('On submit')),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
